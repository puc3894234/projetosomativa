import React, {Component} from 'react';

class App extends Component{

  constructor(props){
    super(props);

    this.state = {
      email: '',
      password: '',
      message: '',
    }

    this.Login = this.Login.bind(this);
   

  }

  Login = () => {
    const { email, password } = this.state;

    if (email === 'eduardo.lino@pucpr.br' && password === '123456') {
      this.setState({ message: 'Acessado com sucesso!' });
    } else {
      this.setState({ message: 'Usuário ou senha incorretos!' });
    }
  };

  render(){
    const { email, password, message } = this.state;

    return (
      <div>
        <h1>Login</h1>
        <input
          type="text"
          placeholder="E-mail"
          value={email}
          onChange={(e) => this.setState({ email: e.target.value })}
        />
        <br/>
        <input
          type="password"
          placeholder="Senha"
          value={password}
          onChange={(e) => this.setState({ password: e.target.value })}
        />
        <br/>
        <button onClick={this.Login}>Acessar</button>
        <br/>
        <label>{message}</label>
      </div>
    );
  }
}


export default App;
  
